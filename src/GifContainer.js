import React, { Fragment } from "react";
import { Typography, Grid, Paper } from "@material-ui/core";

const GifContainer = ({ gif_data }) => {
	if (!!gif_data.id) {
		/* For random gif */
		const { id, title, import_datetime, images: { original: { url }} } = gif_data;
		return (
			<Fragment>
				<Grid key={id} container spacing={3} style={{ width:"89vw", marginLeft:"4.5vw", marginTop:"5vh" }}>
					<Grid item xs={4}></Grid>
					<Grid item xs={4}>
						<Paper style={{ textAlign:"center" }}>
							<Typography variant="h6">
								{ title }
							</Typography>
							<img style={{ maxWidth:"100%" }} src={ url } alt={ title }/>
							<Typography variant="h6">
								{ import_datetime }
							</Typography>
						</Paper>
					</Grid>
				</Grid>
			</Fragment>
		);
	} else if (gif_data.length > 1) {
		/* For Trending or normal search */
		return (
			<Fragment>
				<Grid container spacing={3} style={{ width:"89vw", marginLeft:"4.5vw", marginTop:"5vh" }}>
				{
					gif_data.map((gif, index) => {
						const { id, title, import_datetime, images: { original: { url }} } = gif;
						return (
							<Grid key={id} item xs={3}>
								<Paper style={{ textAlign:"center" }}>
									<Typography variant="h6">
										{ title }
									</Typography>
									<img style={{ maxWidth:"100%" }} src={url} alt={ title }/>
									<Typography variant="h6">
										{ import_datetime }
									</Typography>
								</Paper>
							</Grid>
						);
					})
				}
				</Grid>
			</Fragment>
		);
	}
	return (
		/* For an empty state */
		<Fragment>
			<Grid container spacing={3} style={{ width:"89vw", marginLeft:"4.5vw", marginTop:"5vh" }}>
				<Grid item xs={12}>
					<Paper>
						<Typography variant="h3" style={{ textAlign:"center" }}>
							<br/>(&uArr;)<br/><br/>
							Busca gifs en el cuadro de texto...<br/>
							<br/><br/>
						</Typography>
					</Paper>
				</Grid>
			</Grid>
		</Fragment>
	);
}

export default GifContainer;
