import React, { Component, Fragment } from "react";
import axios from "axios";
import SearchBar from "./SearchBar.js";
import GifContainer from "./GifContainer.js";

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			search_field_value: "",
			gif_data: {}
		}
	}

	search_field_value_changed = (e) => {
		this.setState({
			search_field_value: e.target.value
		});
	}

	search = () => {
		axios.get(`https://api.giphy.com/v1/gifs/search?api_key=KwUAGPkZ66o3hmZNebTTwdmEJxLQNY0e&q=${this.state.search_field_value}&limit=25&offset=0&rating=G&lang=en`)
		.then((response) => {
			this.setState({
				gif_data: response.data.data
			});
		});
	}

	random = () => {
		axios.get(`https://api.giphy.com/v1/gifs/random?api_key=KwUAGPkZ66o3hmZNebTTwdmEJxLQNY0e&tag=${this.state.search_field_value}&rating=G`)
		.then((response) => {
			this.setState({
				gif_data: response.data.data
			});
		});
	}

	trending = () => {
		axios.get(`https://api.giphy.com/v1/gifs/trending?api_key=KwUAGPkZ66o3hmZNebTTwdmEJxLQNY0e&limit=25&offset=0&rating=G`)
		.then((response) => {
			this.setState({
				gif_data: response.data.data
			});
		});
	}

	performSearch = (type, event = undefined) => {
		switch (type) {
			case "search":
				if (event) {
					if (/Enter/.test(event.key)) {
						this.search();
					}
				} else {
					this.search();
				}
				break;
			case "trending":
				this.trending();
				break;
			case "random":
				if (this.state.search_field_value) {
					this.random();
				} else {
					alert("Por favor especifica un Tag en el cuadro de busqueda");
				}
				break;
			default:
				break;
		}
	}

	render() {
		return (
			<Fragment>
				<SearchBar
					search_field_value={ this.state.search_field_value }
					search_field_value_changed={ this.search_field_value_changed }
					performSearch={ this.performSearch }
				/>
				<GifContainer gif_data={ this.state.gif_data } />
			</Fragment>
		);
	}
}

export default App;
