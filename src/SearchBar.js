import React, { Fragment } from "react";
import { TextField, Button, Fab } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

const SearchBar = ({ search_field_value, search_field_value_changed, performSearch }) => {
	return (
		<Fragment>
			<div id="search_bar" className="initial">
				<TextField
					style={{ height:"8%", width:"50vw", marginLeft:"5vw", marginTop:"2vh" }}
					placeholder="Busca un gif..."
					margin="normal"
					variant="outlined"
					autoFocus
					onChange={ search_field_value_changed }
					value={ search_field_value }
					onKeyUp={(e) => { performSearch("search", e); }}
				/>
				<Fab color="primary" style={{ marginTop:"1%", marginLeft:"1%" }} onClick={(e) => { performSearch("search"); }}>
					<SearchIcon />
				</Fab>
				<Button variant="contained" color="primary" style={{ height:"5vh", width:"15vw", marginTop:"1%", marginLeft:"2vw" }} onClick={(e) => { performSearch("trending"); }}>
					Gifs en tendencias
				</Button>
				<Button variant="contained" color="primary" style={{ height:"5vh", width:"15vw", marginTop:"1%", marginLeft:"2vw" }} onClick={(e) => { performSearch("random"); }}>
					Gif aleatorio
				</Button>
			</div>
		</Fragment>
	)
}

export default SearchBar;
